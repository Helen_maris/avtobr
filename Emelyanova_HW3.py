import re
import numpy as np
import csv
import matplotlib
%matplotlib inline
from matplotlib import pyplot as plt
from sklearn import grid_search, svm
from pymystem3 import Mystem

with open('/Users/elenaemelanova/Downloads/anna.txt', encoding='utf-8') as f:
    anna = f.read()
with open('/Users/elenaemelanova/Downloads/sonets.txt', encoding='utf-8') as f:
    sonets = f.read()

anna_sentences = re.split(r'(?:[.]\s*){3}|[.?!]', anna)
sonet_sentences = re.split(r'(?:[.]\s*){3}|[.?!]', sonets)
#print(None in anna_sentences)
#print(len(anna_sentences), len(sonet_sentences))

anna_lengths = []
for sentence in anna_sentences:

    len_letter = []
    length = re.findall('[А-Яа-я]', sentence)
    if length != None:
        t = len(length)
        if t != 0:
            len_letter.append(t)
    ##
    sentence = sentence.lower()
    dif_letters = []
    d = []
    dif_let = re.findall('[а-я]', sentence)
    for i in dif_let:
        if i not in d:
            d.append(i)
    if len(d) != 0:
        len_letter.append(len(d))
        vowels = []
        vow = re.findall('[уеаоэяиюыо]', sentence)
        v = len(vow)
        if v != 0:
            len_letter.append(v)
        med_w = []
        sentence = sentence.split(" ")
        word_sent = []
        word_sent_len = []
        for word in sentence:
            word = word.strip(',?!():;"')
            if word != '':
                if word!= '\n':
                    word_sent.append(word)
        #print(word_sent)
        if len(word_sent) != 0:
            for i in word_sent:
                word_sent_len.append(len(i))
        if len(word_sent_len) != 0:
            #print(word_sent_len)
            g = np.median(word_sent_len)
            len_letter.append(g)
        med_vow = []
        vow_sent = []
        len_vow_sent = []
        for word in sentence:
            word = word.strip(',?!():;"')
            if word != '':
                if word!= '\n':
                    s_vow = re.findall('[уеаоэяиюыо]', word)
                    vow_sent.append(s_vow)
        for i in vow_sent:
            len_vow_sent.append(len(i))
        if len(len_vow_sent):
            k = np.median(len_vow_sent)
            len_letter.append(k)
    len_letter.append(1)
    if len(len_letter) == 6:
        anna_lengths.append(len_letter)

#print(anna_lengths)


sonet_lengths = []
for sentence in sonet_sentences:
    len_letter = []
    length = re.findall('[А-Яа-я]', sentence)
    if length != None:
        t = len(length)
        if t != 0:
            len_letter.append(t)
    ##
    sentence = sentence.lower()
    dif_letters = []
    d = []
    dif_let = re.findall('[а-я]', sentence)
    for i in dif_let:
        if i not in d:
            d.append(i)
    if len(d) != 0:
        len_letter.append(len(d))
    ##
        vowels = []
        vow = re.findall('[уеаоэяиюыо]', sentence)
        v = len(vow)
        if v != 0:
            len_letter.append(v)
    ##
        med_w = []
        sentence = sentence.split(" ")
        word_sent = []
        word_sent_len = []
        for word in sentence:
            word = word.strip(',?!():;"')
            if word != '':
                if word!= '\n':
                    word_sent.append(word)
        #print(word_sent)
        if len(word_sent) != 0:
            for i in word_sent:
                word_sent_len.append(len(i))
        if len(word_sent_len) != 0:
            #print(word_sent_len)
            g = np.median(word_sent_len)
            len_letter.append(g)
    ##
        med_vow = []
        vow_sent = []
        len_vow_sent = []
        for word in sentence:
            word = word.strip(',?!():;"')
            if word != '':
                if word!= '\n':
                    s_vow = re.findall('[уеаоэяиюыо]', word)
                    vow_sent.append(s_vow)
        for i in vow_sent:
            len_vow_sent.append(len(i))
        if len(len_vow_sent):
            k = np.median(len_vow_sent)
            len_letter.append(k)
    len_letter.append(2)
    if len(len_letter) == 6:
        sonet_lengths.append(len_letter)

#print(sonet_lengths)


anna_data = []
sonet_data = []
for i in anna_lengths:
    if len(i) != 0:
        l = tuple(i)
        anna_data.append(l)
print(anna_data)

for i in sonet_lengths:
    if len(i) != 0:
        h = tuple(i)
        sonet_data.append(h)
        
anna_dat = np.array(anna_data)
sonet_dat = np.array(sonet_data)

plt.figure()
plt.plot(anna_dat[:,0], anna_dat[:,1], 'og',
         sonet_dat[:,0], sonet_dat[:,1], 'sb')
plt.show()


from sklearn.model_selection import train_test_split
test, train = train_test_split(data, random_state=3, test_size=0.3)


parameters = {'C': (.1, .05, 0.04, 0.03, 0.2, 0.01)}
gs = grid_search.GridSearchCV(svm.LinearSVC(), parameters)
gs.fit(data[:, :4], data[:, 5])
print('Best result is ',gs.best_score_)
print('Best C is', gs.best_estimator_.C)
clf = svm.LinearSVC(C=gs.best_estimator_.C)
clf.fit(train[:, :4], train[:, 5])


wrong = 0
answ = []
for obj in test[:,:]:

    label = clf.predict(obj[:4]).item(0)
    #print(label != obj[5])
    #print(label)
    #print(obj[5])
    answ.append((label,obj[5]))
    if float(label) != float(obj[5]):
        print('Пример ошибки машины: class = ', obj[5], ', label = ', label, ', экземпляр ', obj[:4])
        wrong += 1
    if wrong > 2:
        break
        
#Пример ошибки машины: class =  2.0 , label =  1.0 , экземпляр  [ 55.   22.   21.    5.5]
#Пример ошибки машины: class =  2.0 , label =  1.0 , экземпляр  [ 48.   16.   21.    6.5]
#Пример ошибки машины: class =  2.0 , label =  1.0 , экземпляр  [ 56.  21.  21.   7.]